import React from 'react'
import { StyleSheet, Image, Text, View, ScrollView} from 'react-native'
import ab from "../../assets/images/ab.jpg"
import ac from "../../assets/images/ac.jpg"
import ad from "../../assets/images/ad.jpg"
import ae from "../../assets/images/ae.jpg"
import af from "../../assets/images/af.jpg"
import ag from "../../assets/images/ag.jpg"
import ai from "../../assets/images/ai.jpg"
import ah from "../../assets/images/ah.jpg"


const Foto = (props) => {
  return (
    <View style={styles.isi}>
      <Image 
        source={props.gambar} 
        style={styles.gambar}
      />
      <Text style={styles.nama}>{props.nama}</Text>
      <Text style={styles.jabatan}>{props.jabatan}</Text>
    </View>
  )
}

const Karyawan = (props) => {
  return (
    <View>
      <Text style={styles.judul}>WISATA DI BANGKALAN</Text>
      <ScrollView>
        <View style={styles.wadah}>
          <Foto nama="Bukit Kapur jeddih" jabatan="desa pasreh,kec.socah" gambar={ab}/>
          <Foto nama="Pantai siring kemuning" jabatan="desa mecajah,kec.tanjung bumi" gambar={ac}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="aer ibu arosbaya" jabatan="Desa buduran,kec.Arosbaya" gambar={ad}/>
          <Foto nama="Air terjun batu raja manitan" jabatan="dusun runggarung,desa.banyumunih,kec.galis" gambar={ae}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="pantai rongkang" jabatan="desa kwanyar" gambar={af}/>
          <Foto nama="masjid agung " jabatan="jln.kh.kholil,demangan" gambar={ag}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="pasarean makam sultan abdulkadirun" jabatan="" gambar={ai}/>
          <Foto nama="Bukit lampion beramah" jabatan="" gambar={ah}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="pasarean makam sultan abdulkadirun" jabatan="" gambar={ai}/>
          <Foto nama="Bukit lampion beramah" jabatan="" gambar={ah}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="pasarean makam sultan abdulkadirun" jabatan="" gambar={ai}/>
          <Foto nama="Bukit lampion beramah" jabatan="" gambar={ah}/>
        </View>
      </ScrollView>
    </View>
  )
}

export default Karyawan

const styles = StyleSheet.create({
  judul:{
    alignSelf:'center',
    marginVertical:10,
    fontWeight:'bold',
  },
  wadah:{
    width:'100%',
    height:130,
    flexDirection:'row',
    padding:10,
    // borderWidth:1,
  },
  isi:{
    flex:1,
    marginHorizontal:5,
    height:'100%',
    // borderWidth:1,
  },
  gambar:{
    width:'100%',
    height:78,
    borderRadius:5,
  },
  nama:{
    fontWeight:'bold',
    color:'black',
    letterSpacing:2,
  },
  jabatan:{
    fontSize:10,
    letterSpacing:1,
    color:'grey'
  }
})