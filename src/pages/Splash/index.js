import React, {useEffect, useState} from 'react';
import { View, Text, Image, StyleSheet, StatusBar} from 'react-native';

const Splash = () => {
    return (
        <View style={style.back}>
            <StatusBar backgroundColor={'black'} barStyle='light-content' />
            <Image
                source={require('../../assets/images/logo2.jpg')}
                style={style.backg}
            /> 
            <Text style={style.text}>WISATA ALAM & RELIGI PULAU MADURA</Text>
        </View>
    );
};

export default Splash
const style = StyleSheet.create ({
    text:{
        alignSelf:'center',
        color:'black'
    },
    back:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    backg:{
        width:400,
        height:670,
    }
})