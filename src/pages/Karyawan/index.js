import React from 'react'
import { StyleSheet, Image, Text, View, ScrollView} from 'react-native'
import camplong from "../../assets/images/camplong.jpg"
import ratu from "../../assets/images/ratu.jpg"
import goa from "../../assets/images/goa.jpg"
import klampis from "../../assets/images/klampis.jpg"
import toroan from "../../assets/images/toroan.jpg"
import jodoh from "../../assets/images/jodoh.jpg"
import nepa from "../../assets/images/nepa.jpg"
import profil6 from "../../assets/images/profil6.jpg"


const Foto = (props) => {
  return (
    <View style={styles.isi}>
      <Image 
        source={props.gambar} 
        style={styles.gambar}
      />
      <Text style={styles.nama}>{props.nama}</Text>
      <Text style={styles.jabatan}>{props.jabatan}</Text>
    </View>
  )
}

const Karyawan = (props) => {
  return (
    <View>
      <Text style={styles.judul}>WISATA DI SAMPANG</Text>
      <ScrollView>
        <View style={styles.wadah}>
          <Foto nama="PANTAI CAMPLONG" jabatan="desa dharma camplong kab.sampang" gambar={camplong}/>
          <Foto nama="MAKAM RATU IBU" jabatan="Desa polagan,kab.sampang" gambar={ratu}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="GOA LEBAR" jabatan="jl.pahlawan,desa rong tengah kec.sampang" gambar={goa}/>
          <Foto nama="WADUK KLAMPIS" jabatan="desa.kramat, Kec.kedudung,9 km sampang" gambar={klampis}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="AIR TERJUN TOROAN" jabatan="Desa ketapang daya, kec.ketapang, kab sampang" gambar={toroan}/>
          <Foto nama="PANTAI NEPA" jabatan="DESA.batioh,kec.ketapang,kab.sampang " gambar={nepa}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="PANTAI JODOH" jabatan="Desa bira timur,kec.sokobanah,kab.sampang" gambar={jodoh}/>
          <Foto nama="........" jabatan="........" gambar={jodoh}/>
        </View>
      </ScrollView>
    </View>
  )
}

export default Karyawan

const styles = StyleSheet.create({
  judul:{
    alignSelf:'center',
    marginVertical:10,
    fontWeight:'bold',
  },
  wadah:{
    width:'100%',
    height:130,
    flexDirection:'row',
    padding:10,
    // borderWidth:1,
  },
  isi:{
    flex:1,
    marginHorizontal:5,
    height:'100%',
    // borderWidth:1,
  },
  gambar:{
    width:'100%',
    height:78,
    borderRadius:5,
  },
  nama:{
    fontWeight:'bold',
    color:'black',
    letterSpacing:2,
  },
  jabatan:{
    fontSize:10,
    letterSpacing:1,
    color:'grey'
  }
})