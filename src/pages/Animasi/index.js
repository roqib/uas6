import React from 'react'
import { StyleSheet, Image, Text, View, ScrollView} from 'react-native'
import aa from "../../assets/images/aa.jpg"
import bb from "../../assets/images/bb.jpg"
import cc from "../../assets/images/cc.jpg"
import dd from "../../assets/images/dd.jpg"
import ee from "../../assets/images/ee.jpg"
import kk from "../../assets/images/kk.jpg"
import ff from "../../assets/images/ff.jpg"
import gg from "../../assets/images/gg.jpg"
import ii from "../../assets/images/ii.jpg"
import jj from "../../assets/images/jj.jpg"
import mm from "../../assets/images/mm.jpg"
import nn from "../../assets/images/nn.jpg"
import oo from "../../assets/images/oo.jpg"
import pp from "../../assets/images/pp.jpg"


const Foto = (props) => {
  return (
    <View style={styles.isi}>
      <Image 
        source={props.gambar} 
        style={styles.gambar}
      />
      <Text style={styles.nama}>{props.nama}</Text>
      <Text style={styles.jabatan}>{props.jabatan}</Text>
    </View>
  )
}

const Karyawan = (props) => {
  return (
    <View>
      <Text style={styles.judul}>WISATA DI PAMEKASAN</Text>
      <ScrollView>
        <View style={styles.wadah}>
          <Foto nama="Api Tak Kunjung Padam" jabatan="lokasi di desa larangan,kab pamekasan" gambar={aa}/>
          <Foto nama="Pantai Talang" jabatan="berada di Desa Montok, Kec Larangan, Kab Pamekasan." gambar={bb}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="Puncak RATU" jabatan="........" gambar={cc}/>
          <Foto nama="PANTAI JUMIANG" jabatan="di Desa Tanjung, Kec Pademawu, Kab Pamekasan" gambar={dd}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="Viara " jabatan=".............." gambar={ee}/>
          <Foto nama="Kampung toron s" jabatan="..........." gambar={ff}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="kampung durian" jabatan=".............." gambar={gg}/>
          <Foto nama="maggrop lembun" jabatan=".............." gambar={ii}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="eduwisata garam" jabatan=".............." gambar={jj}/>
          <Foto nama="bukit kehi" jabatan="kertagenah daya,kadur,kec pamekasan" gambar={mm}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="pantai cemara" jabatan=".............." gambar={nn}/>
          <Foto nama="Bukit cinta" jabatan="................" gambar={oo}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="Air terjun durbugen" jabatan="di Kelurahan Kowel, Pamekasan" gambar={pp}/>
          <Foto nama="Kampung toron s" jabatan="........." gambar={ff}/>
        </View>
      </ScrollView>
    </View>
  )
}

export default Karyawan

const styles = StyleSheet.create({
  judul:{
    alignSelf:'center',
    marginVertical:10,
    fontWeight:'bold',
  },
  wadah:{
    width:'100%',
    height:130,
    flexDirection:'row',
    padding:10,
    // borderWidth:1,
  },
  isi:{
    flex:1,
    marginHorizontal:5,
    height:'100%',
    // borderWidth:1,
  },
  gambar:{
    width:'100%',
    height:78,
    borderRadius:5,
  },
  nama:{
    fontWeight:'bold',
    color:'black',
    letterSpacing:2,
  },
  jabatan:{
    fontSize:10,
    letterSpacing:1,
    color:'grey'
  }
})