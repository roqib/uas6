import React from 'react'
import { StyleSheet, Image, Text, View, ScrollView} from 'react-native'
import OIP from "../../assets/images/OIP.jpg"
import sembilan from "../../assets/images/sembilan.jpg"
import agung from "../../assets/images/agung.jpg"
import masalembu from "../../assets/images/masalembu.jpg"
import terjun from "../../assets/images/terjun.jpg"
import slopeng from "../../assets/images/slopeng.jpg" 
import lombang from "../../assets/images/lombang.jpg"
import genting from "../../assets/images/genting.jpg"
import kraton from "../../assets/images/kraton.jpg"
import tinggi from "../../assets/images/tinggi.jpg"
import ambuten from "../../assets/images/ambuten.jpg"




const Foto = (props) => {
  return (
    <View style={styles.isi}>
      <Image 
        source={props.gambar} 
        style={styles.gambar}
      />
      <Text style={styles.nama}>{props.nama}</Text>
      <Text style={styles.jabatan}>{props.jabatan}</Text>
    </View>
  )
}

const Karyawan = (props) => {
  return (
    <View>
      <Text style={styles.judul}>WISATA DI SUMENEP</Text>
      <ScrollView>
        <View style={styles.wadah}>
          <Foto nama="PANTAI GILI GENTING" jabatan="lokasi kecamatan gili genting kab.sumenep" gambar={OIP}/>
          <Foto nama="PANTAI SEMBILAN" jabatan="JL.bringsang, kec.giligenteng kab.sumeneps" gambar={sembilan}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="MASJID AGUNG" jabatan="Jln trunojoyo,No .354,bandselok,malang,dalem anyar kab.sumenep" gambar={agung}/>
          <Foto nama="PANTAI GILI GENTING" jabatan="lokasi kecamatan gili genting kab.sumenep" gambar={masalembu}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="AIR TERJUN DURBUGAN" jabatan="desa aeng merah ,kec.batu putih, kab.sumenep" gambar={terjun}/>
          <Foto nama="PANTAI SLOPENG" jabatan="JL.Ambuten,No.34,desa semmaan,kec.dasuk kab.sumenep" gambar={slopeng}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="PANTAI LOMBANG" jabatan="KEC.BATANG-BATANG kab.sumenep" gambar={lombang}/>
          <Foto nama="PANTAI AMBUTEN" jabatan="Desa ambunteb barat,kec.ambuten,kab.sumenep" gambar={ambuten}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="PANTAI GILI GENTING" jabatan="lokasi kecamatan gili genting kab.sumenep" gambar={genting}/>
          <Foto nama="MUSIUM KRATON" jabatan="JL.DR Stomo nmr 6, lingkungan delama,pajagalan kab.sumenep" gambar={kraton}/>
        </View>
        <View style={styles.wadah}>
          <Foto nama="PANTAI GILI GENTING" jabatan="lokasi kecamatan gili genting kab.sumenep" gambar={tinggi}/>
          <Foto nama="PANTAI GILI GENTING" jabatan="lokasi kecamatan gili genting kab.sumenep" gambar={ambuten}/>
        </View>
      </ScrollView>
    </View>
  )
}

export default Karyawan

const styles = StyleSheet.create({
  judul:{
    alignSelf:'center',
    marginVertical:10,
    fontWeight:'bold',
  },
  wadah:{
    width:'100%',
    height:130,
    flexDirection:'row',
    padding:10,
    // borderWidth:1,
  },
  isi:{
    flex:1,
    marginHorizontal:5,
    height:'100%',
    // borderWidth:1,
  },
  gambar:{
    width:'100%',
    height:78,
    borderRadius:5,
  },
  nama:{
    fontWeight:'bold',
    color:'black',
    letterSpacing:2,
  },
  jabatan:{
    fontSize:10,
    letterSpacing:1,
    color:'grey'
  }
})